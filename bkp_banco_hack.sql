-- MySQL dump 10.13  Distrib 5.6.40, for Win32 (AMD64)
--
-- Host: localhost    Database: hack
-- ------------------------------------------------------
-- Server version	5.6.40-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Laticíneos','não tem'),(2,'Mercearia','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/mercearia.png?alt=media&token=6b2d9410-700f-46c5-9198-c51507be5140'),(3,'Bebidas','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/bebidas.png?alt=media&token=89e3ef7d-5ca3-4e20-a954-ff113f9ecda4'),(4,'HortiFruti','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/hortifruti.png?alt=media&token=a8417998-f8b2-47f3-9487-bd6a96c9fbca'),(5,'Higiene Pessoal','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/Higiene%20Pessoal.png?alt=media&token=9a9d8ece-f321-4bef-a663-025ace49fba0 ');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `client_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1),(1),(1),(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market`
--

DROP TABLE IF EXISTS `market`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market` (
  `market_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cnpj` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `market_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`market_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market`
--

LOCK TABLES `market` WRITE;
/*!40000 ALTER TABLE `market` DISABLE KEYS */;
INSERT INTO `market` VALUES (1,'01.123.321/0001-9','(19) 3231-1234','sac@paodeacucar.com.br','1234568','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pda.png?alt=media&token=5630b949-709b-4e42-8900-08c20c471f6d','4568','Pão de Açúcar'),(2,'01.321.456/0001-98','(19) 3214-9854','sac@carrefour.com.br','321321','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/carrefour.png?alt=media&token=15a29617-f8c5-451c-93fc-b7e0734884f5','6544','Carrefour'),(3,'01.433.222/0001-80','(19) 3224-4354','sac@dalben.com.br','32435345','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/dalben.png?alt=media&token=33879baa-9c9a-4a45-bda4-a70ea9e1499b','5434543','Dalben'),(4,'01.122.756/0001-09','(19) 3555-1353','sac@extra.com.br','324445','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/extra.png?alt=media&token=0d36be04-2f3d-41bb-b1e3-b574d6a0cd49','54341113','Extra'),(5,'01.1324.721/0004-19','(19) 3554-1123','sac@oba.com.br','325454','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/oba.png?alt=media&token=f7b060c5-c708-4d46-b259-537a2752bee7','577777','Oba'),(6,'01.122.865/0034-13','(19) 3554-1123','sac@walmart.com.br','3252884','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/walmart.png?alt=media&token=f11fbc3d-24d5-4237-a17d-ab7764211382','57999999','Walmart');
/*!40000 ALTER TABLE `market` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expiration_date` datetime DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `original_price` double NOT NULL,
  `price` double NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) NOT NULL,
  `market_id` bigint(20) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `FK1cf90etcu98x1e6n9aks3tel3` (`category_id`),
  KEY `FK7tk81w8y297mrudqam2ef3gjr` (`market_id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'2018-08-23 00:00:00','logo',1.98,0.75,'Iogurte Marba',1,1),(2,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.49,1,'Brahma',3,1),(3,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.59,1,'Brahma',3,2),(4,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.5,0.9,'Brahma',3,3),(5,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.5,0.99,'Brahma',3,4),(6,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.59,0.98,'Brahma',3,5),(7,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.79,0.99,'Brahma',3,6),(8,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.5,0.99,'Suco de Criança',3,6),(9,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.59,0.98,'Suco de Criança',3,5),(10,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.59,0.98,'Suco de Criança',3,5),(11,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.4,0.9,'Suco de Criança',3,4),(12,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.5,0.8,'Suco de Criança',3,3),(13,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.7,1,'Suco de Criança',3,2),(14,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/cerveja.jpg?alt=media&token=5509333a-3f70-41c2-97cf-eeb245189be0',2.6,0.9,'Suco de Criança',3,1),(15,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.6,0.9,'Suco Del Valle',3,1),(16,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.5,0.8,'Suco Del Valle',3,2),(17,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.5,1,'Suco Del Valle',3,3),(18,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.8,1,'Suco Del Valle',3,4),(19,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.6,0.9,'Suco Del Valle',3,5),(20,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/suco.jpg?alt=media&token=75c1b3a7-001e-4323-8d33-14f6b0840cc9',2.56,0.9,'Suco Del Valle',3,6),(21,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',1.9,0.9,'Macarrão',2,6),(22,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',1.9,0.8,'Macarrão',2,5),(23,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',1.99,0.89,'Macarrão',2,4),(24,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',2,0.91,'Macarrão',2,3),(25,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',2.32,0.9,'Macarrão',2,2),(26,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/macarrao.jpg?alt=media&token=3679ead0-faf6-4e3d-ad26-19ed67f888ab',2.32,0.9,'Macarrão',2,1),(27,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15,5,'Pão',2,1),(28,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15,5.2,'Pão',2,2),(29,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15.6,5.2,'Pão',2,3),(30,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15.7,5.2,'Pão',2,4),(31,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15.8,5,'Pão',2,5),(32,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/pao.jpeg?alt=media&token=9f799b03-01e1-4274-bb28-6d35fad2ca03',15.9,5.1,'Pão',2,6),(33,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',6,2,'Croissant',2,6),(34,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',5,2,'Croissant',2,5),(35,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',5,1,'Croissant',2,4),(36,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',5,1.6,'Croissant',2,3),(37,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',5,1.8,'Croissant',2,2),(38,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/crossaint.jpg?alt=media&token=cc94c095-da93-411c-82a9-dfe522b0df83',5.6,1.8,'Croissant',2,1),(39,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2.4,0.5,'Alface',4,1),(40,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2.5,0.5,'Alface',4,2),(41,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2.5,0.4,'Alface',4,3),(42,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2.5,0.4,'Alface',4,4),(43,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2.7,0.5,'Alface',4,5),(44,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/alface.jpg?alt=media&token=e734892f-f6b4-4f3a-8b43-a7a86c109e7d',2,0.3,'Alface',4,6),(45,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',2,0.3,'Repolho',4,6),(46,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',2,0.5,'Repolho',4,5),(47,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',1.8,0.4,'Repolho',4,4),(48,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',1.9,0.4,'Repolho',4,3),(49,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',1.9,0.7,'Repolho',4,2),(50,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/repolho.jpg?alt=media&token=0e6bf891-7366-429b-8686-a5cbc421adba',1.3,0.3,'Repolho',4,1),(51,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1.3,0.3,'Rúcula',4,1),(52,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1.5,0.5,'Rúcula',4,2),(53,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1.2,0.4,'Rúcula',4,3),(54,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1,0.5,'Rúcula',4,4),(55,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1,0.45,'Rúcula',4,5),(56,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/rucula.jpg?alt=media&token=ac99b27a-dd84-4c34-a822-05e44f1b7e42',1.1,0.45,'Rúcula',4,6),(57,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1.1,0.45,'Sabonete Líquido',5,6),(58,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1,0.55,'Sabonete Líquido',5,5),(59,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1,0.45,'Sabonete Líquido',5,4),(60,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1,0.6,'Sabonete Líquido',5,3),(61,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1.2,0.5,'Sabonete Líquido',5,2),(62,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete%20liquido.jpg?alt=media&token=e41e8eed-afda-4e73-8c25-57b77046e3f7',1.25,0.55,'Sabonete Líquido',5,1),(63,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.25,0.55,'Sabonete',5,1),(64,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.35,0.55,'Sabonete',5,2),(65,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.36,0.56,'Sabonete',5,3),(66,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.5,0.5,'Sabonete',5,4),(67,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.8,0.6,'Sabonete',5,5),(68,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/sabonete.jpg?alt=media&token=31e420ad-1624-4aaa-9a6a-b3eae3cf6fed',1.2,0.3,'Sabonete',5,6),(69,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',5.5,0.3,'Shampoo',5,6),(70,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',5.8,1.9,'Shampoo',5,5),(71,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',6.8,2,'Shampoo',5,4),(72,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',6.5,2,'Shampoo',5,3),(73,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',5.5,1.2,'Shampoo',5,3),(74,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',5,1.2,'Shampoo',5,2),(75,'2018-08-23 00:00:00','https://firebasestorage.googleapis.com/v0/b/movilehack.appspot.com/o/shampoo.jpg?alt=media&token=cf882d76-82a1-401b-9407-f921fbb0bd1d',5.8,1,'Shampoo',5,1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-18 23:39:55
