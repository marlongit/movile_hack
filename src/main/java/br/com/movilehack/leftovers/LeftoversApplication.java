package br.com.movilehack.leftovers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeftoversApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeftoversApplication.class, args);
	}
	
}
