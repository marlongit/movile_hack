package br.com.movilehack.leftovers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.com.movilehack.leftovers.entity.Category;
import br.com.movilehack.leftovers.service.CategoryService;

@RestController
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@CrossOrigin
	@GetMapping("/market/{marketId}/category")
	public Iterable<Category> getCategories(@PathVariable Integer marketId) {
		
		Iterable<Category> categories = categoryService.findByMarket(marketId);
		return categories;
	}
}
