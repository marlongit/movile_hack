package br.com.movilehack.leftovers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.movilehack.leftovers.entity.Market;
import br.com.movilehack.leftovers.entity.Product;
import br.com.movilehack.leftovers.service.MarketService;

@RestController
public class MarketController {
	
	@Autowired
	private MarketService marketService;
	
	@CrossOrigin
	@GetMapping("/markets")
	public Iterable<Market> getMarkets() {
		Iterable<Market> markets = marketService.findAll();
		return markets;
	}
	
	@CrossOrigin
	@PostMapping("/market/{marketId}/closeOrder")
	public ResponseEntity<?> closeOrder(@PathVariable Integer marketId, @RequestBody List<Product> products) {
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
}
