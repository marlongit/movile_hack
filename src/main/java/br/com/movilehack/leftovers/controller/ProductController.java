package br.com.movilehack.leftovers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.com.movilehack.leftovers.entity.Product;
import br.com.movilehack.leftovers.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@CrossOrigin
	@GetMapping("/market/{marketId}/category/{categoryId}/products")
	public Iterable<Product> getProducts(@PathVariable Integer marketId, @PathVariable Integer categoryId) {
		Iterable<Product> products = productService.findByMarketAndCategory(marketId, categoryId);
		return products;
	}
	
	@CrossOrigin
	@GetMapping("/market/{marketId}/bestDeals")
	public Iterable<Product> getBestDeals(@PathVariable Integer marketId) {
		Iterable<Product> products = productService.findBestDeals(marketId);
		return products;
	}
}
