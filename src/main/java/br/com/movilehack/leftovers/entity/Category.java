package br.com.movilehack.leftovers.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="category")
public class Category {

	@Id
	@GeneratedValue
	private Long categoryId;
	private String categoryName;
	private String imageUrl;
	
	Category(){
	
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategory_id(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
