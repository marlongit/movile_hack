package br.com.movilehack.leftovers.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="Customer")
public class Customer {
	
	@Id
	@GeneratedValue
	private Long clientId;
	private String name;
	private String address;
	private String email;
	private String contact;

	Customer() {
	
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClient_id(Long clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

}
