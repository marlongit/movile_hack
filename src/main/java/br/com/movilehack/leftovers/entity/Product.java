package br.com.movilehack.leftovers.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="products")
public class Product {
	
	@Id
	@GeneratedValue
	private long productId;
	private String productName;
	private double originalPrice;
	private double price;
	private String imageUrl;
	private Date expirationDate;
	
	@ManyToOne
	@JoinColumn(name="categoryId", nullable=false)
	private Category category;
	@ManyToOne
	@JoinColumn(name="marketId", nullable=false)
	private Market market;
	
	Product(){
		
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String image) {
		this.imageUrl = image;
	}

	public Category getCategoryId() {
		return category;
	}

	public void setCategoryId(Category category) {
		this.category = category;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarketId(Market market) {
		this.market = market;
	}

}
