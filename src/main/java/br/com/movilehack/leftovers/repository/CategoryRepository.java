package br.com.movilehack.leftovers.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.movilehack.leftovers.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {
	
	@Query(value = "SELECT * FROM Category c INNER JOIN Products p ON p.category_id = c.category_id INNER JOIN Market m ON m.market_id = p.market_id WHERE m.market_id = ?1", nativeQuery = true)
	Iterable<Category> findByMarket(Integer marketId);
}
