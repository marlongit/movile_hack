package br.com.movilehack.leftovers.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.movilehack.leftovers.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
	
}
