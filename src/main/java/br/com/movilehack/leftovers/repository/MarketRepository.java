package br.com.movilehack.leftovers.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.movilehack.leftovers.entity.Market;

public interface MarketRepository extends CrudRepository<Market, Long> {
	
}
