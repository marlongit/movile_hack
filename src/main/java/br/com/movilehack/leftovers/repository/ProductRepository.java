package br.com.movilehack.leftovers.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.movilehack.leftovers.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
	
	@Query(value = "SELECT * FROM Products p INNER JOIN Market m ON m.market_id = p.market_id INNER JOIN Category c ON c.category_id = p.category_id WHERE m.market_id = ?1 AND c.category_id = ?2", nativeQuery = true)
	public Iterable<Product> findByMarketAndCategory(Integer marketId, Integer categoryId);

	@Query(value = "SELECT * FROM Products p INNER JOIN Market m ON m.market_id = p.market.market_id WHERE m.market_id = ?1 ORDER BY p.price LIMIT 5", nativeQuery = true)
	public Iterable<Product> findBestDeals(Integer marketId);
}
