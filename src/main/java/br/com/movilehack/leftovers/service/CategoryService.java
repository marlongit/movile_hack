package br.com.movilehack.leftovers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.movilehack.leftovers.entity.Category;
import br.com.movilehack.leftovers.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public Iterable<Category> findByMarket(Integer marketId) {
		Iterable<Category> categories = categoryRepository.findByMarket(marketId);
		return categories;
	}
}
