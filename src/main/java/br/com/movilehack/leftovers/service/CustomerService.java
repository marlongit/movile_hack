package br.com.movilehack.leftovers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.movilehack.leftovers.entity.Customer;
import br.com.movilehack.leftovers.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public Iterable<Customer> findAll() {
		Iterable<Customer> customers = customerRepository.findAll();
		return customers;
	}

	public void save(Customer customer) {
		customerRepository.save(customer);
	}
}
