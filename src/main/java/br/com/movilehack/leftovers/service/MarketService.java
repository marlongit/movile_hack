package br.com.movilehack.leftovers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.movilehack.leftovers.entity.Market;
import br.com.movilehack.leftovers.repository.MarketRepository;


@Service
public class MarketService {
	
	@Autowired
	private MarketRepository marketRepository;

	public Iterable<Market> findAll() {
		Iterable<Market> markets = marketRepository.findAll();
		return markets;
	}

	public void save(Market market) {
		marketRepository.save(market);
	}
}
