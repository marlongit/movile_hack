package br.com.movilehack.leftovers.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.movilehack.leftovers.entity.Product;
import br.com.movilehack.leftovers.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	public Iterable<Product> findByMarketAndCategory(Integer marketId, Integer categoryId) {
		Iterable<Product> products = productRepository.findByMarketAndCategory(marketId, categoryId);
		return products;
	}

	public Iterable<Product> findBestDeals(Integer marketId) {
		Iterable<Product> products = productRepository.findBestDeals(marketId);
		return products;
	}
}
